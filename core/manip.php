<?php

class manip {

	function createFile($filename, $location = ''){
		$FileHandle = fopen($location.$filename, 'w') or die($return = 0);
		fclose($FileHandle);		
		if (isset($return)) {
			return 0;
		}else{
			return 1;
		}
	}
	
	function createFileFTP($fpc_path_and_name, $fpc_content) {
    
    //Temporary folder in the server
    $cfg_temp_folder = str_replace("//", "/", $_SERVER['DOCUMENT_ROOT']."/_temp/");
    
    //Link to FTP
    $cfg_ftp_server = "ftp://ftp.com";
    
    //FTP username
    $cfg_user = "user";
    
    //FTP password
    $cfg_pass = "password";
    
    //Document Root of FTP
    $cfg_document_root = "DOCUMENT ROOT OF FTP";
    
    //Link to the website
    $cfg_site_link = "Link to the website";
    
    //Check if conteins slash on the path of the file
    $cotains_slash = strstr($fpc_path_and_name, "/");
    
    //Get filename and paths
    if ($cotains_slash) {
        $fpc_path_and_name_array = explode("/", $fpc_path_and_name);
        $fpc_file_name = end($fpc_path_and_name_array);
    }
    else {
        $fpc_file_name = $fpc_path_and_name;
    }
    
    //Create local temp dir
    if (!file_exists($cfg_temp_folder)) {
        if (!mkdir($cfg_temp_folder, 0777)) {
            echo "Unable to generate a temporary folder on the local server - $cfg_temp_folder.<br />";
            die();
        }
    }
    
    //Create local file in temp dir
    if (!file_put_contents(str_replace("//", "/", $cfg_temp_folder.$fpc_file_name), $fpc_content)) {
        echo "Unable to generate the file in the temporary location - ".str_replace("//", "/", $cfg_temp_folder.$fpc_file_name).".<br />";
        die();
    }

    //Connection to the FTP Server
    $fpc_ftp_conn = ftp_connect("$cfg_ftp_server");
    
    //Check connection
    if (!$fpc_ftp_conn) {
        echo "Could not connect to server <b>$cfg_ftp_server</b>.<br />";
        die();
    }
    else {
        
        // login
        // check username and password
        if (!ftp_login($fpc_ftp_conn, "$cfg_user", "$cfg_pass")) {
            echo "User or password.<br />";
            die();
        }
        else {
            
            //Document Root
            if (!ftp_chdir($fpc_ftp_conn, $cfg_document_root)) {
                echo "Error to set Document Root.<br />";
                die();
            }
            
            
            //Check if there are folders to create
            if ($cotains_slash) {
                
                //Check if have folders and is not just the file name
                if (count($fpc_path_and_name_array) > 1) {
                    
                    //Remove last array
                    $fpc_remove_last_array = array_pop($fpc_path_and_name_array);
                    
                    //Checks if there slashs on the path
                    if (substr($fpc_path_and_name,0,1) == "/") {
                        $fpc_remove_first_array = array_shift($fpc_path_and_name_array);
                    }
                    
                    //Create each folder on ftp
                    foreach ($fpc_path_and_name_array as $fpc_ftp_path) {
                        if (!@ftp_chdir($fpc_ftp_conn, $fpc_ftp_path)) {
                            if (!ftp_mkdir($fpc_ftp_conn, $fpc_ftp_path)) {
                                echo "Error creating directory $fpc_ftp_path.<br />";
                            }
                            else {
                                if (!ftp_chdir($fpc_ftp_conn, $fpc_ftp_path)) {
                                    echo "Error go to the directory $fpc_ftp_path.<br />";
                                }
                            }
                        }
                    }
                }
                else {
                    
                }
            }
            
            //Check upload file
            if (!ftp_put($fpc_ftp_conn, $fpc_file_name, str_replace("//", "/", $cfg_temp_folder.$fpc_file_name), FTP_ASCII)) {
                echo "File upload <b>$fpc_path_and_name</b> failed!<br />";
                die();
            }
            else {
                if (!unlink(str_replace("//", "/", $cfg_temp_folder.$fpc_file_name))) {
                    echo "Error deleting temporary file.<br />";
                    die();
                }
                else {
                    echo "File upload <a href='$cfg_site_link".str_replace("//", "/", "/$fpc_path_and_name")."'><b>$cfg_site_link".str_replace("//", "/", "/$fpc_path_and_name")."</a></b> successfully performed.<br />";
                }
            }
            
            //Close connection to FTP server
            ftp_close($fpc_ftp_conn);
        	}
    	}
	}
function search($array, $key, $value){
		
    $results = array();

    	if (is_array($array)){
        	if ($array[$key] == $value)
            	$results[] = $array;

        	foreach ($array as $subarray)
            	$results = array_merge($results, search($subarray, $key, $value));
    	}
    	return $results;
}
	
}