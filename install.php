<?php
include('core/manip.php');
$manip = new manip();

if(!isset($_GET['step'])){
	header("Location:install.php?step=1");
}
if (isset($_GET['dbchk'])){
	if($_POST['pass']==NULL){
		$_POST['pass']='';
	}
	if($_GET['dbchk']=="1"){
		$link = mysql_connect($_POST['host'], $_POST['user'], $_POST['pass']) or die(header("Location:install.php?step=2&dbchk=2"));
	if (!$link) {
    	die('Could not connect: ' . mysql_error());
	}
		$db = mysql_select_db($_POST['name']) or die(header("Location:install.php?step=2&dbchk=3"));
	if (!$db) {
		die('Could not Connect to database.');
	}
		echo 'Connected successfully';
		mysql_close($link);
	}
}
if (isset($_GET['do'])) {
		if ($_GET['do']=="1"){
			/* Once I actually do the self-doing Config Generator 
			the code will go here :) until then let's just move the
			user to the intermediate step. BUT IT LOOKS LIKE I HAVE
			TO PUT IN THE USER FILE REGARDLESS *groan* 
			
			$manip->createFile('users.php','admin/users/');
			
			$fileloc = 'admin/users/users.php';

			$filecontents = file_get_contents($fileloc);
			
			$filecontents = array( '1' => array ( 'user' => $_POST['user'], 'pass' => md5($_POST['pass']), 'email' => $_POST['email'] ) );
			$arrayd = print_r($filecontents);
			
			$create = $manip->createFile('users.php','admin/users/');
			
			$fileloc = 'admin/users/users.php';
			
			file_put_contents($fileloc,$arrayd, FILE_APPEND | LOCK_EX); 
			if( $create == 0 ){
			
			$crchk = 0;
			}elseif($create == 1) {
			$crchk = 1;
			}
			
			Or not ;)
			
			*/
		}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>quickSys Install</title><link rel="stylesheet" type="text/css" href="admin/style/adminstyle.css"/>
<script src="SpryAssets/SpryEffects.js" type="text/javascript"></script>
<script type="text/javascript">
function MM_effectSlide(targetElement, duration, from, to, toggle)
{
	Spry.Effect.DoSlide(targetElement, {duration: duration, from: from, to: to, toggle: toggle});
}
</script>
</head>

<body onload="MM_effectSlide('content', 1000, '0%', '100%', false)" onunload="MM_effectSlide('content', 1000, '100%', '0%', false)">
<div id="wrapper" class="head">
	<div id="position">
    <h1>quickSys Install :: <?php if(isset($_GET['step'])){ if($_GET['step']=="1"){ ?>Step 1<?php } if($_GET['step']=="2"){ ?>Step 2<?php } } ?></h1>
    <p class="adminright"><a href="http://quicksys.mattsowden.com/">http://quicksys.mattsowden.com/</a></p>
    </div>
</div>
<div id="content">
<div id="wrapper">
	<div id="position" class="admin">
    <h1>Install quickSys</h1>
    <p>&nbsp;</p>
    <?php if(isset($_GET['step'])){
		if($_GET['step']=="1"){ ?>
    <p>Hello! Thanks for downloading quickSys! Before you can jump into the interface we just need a bit of information to get started!</p>
    <p>&nbsp;</p>
    <h2>Checking Required Software</h2>
    <table width="100%" border="1" cellspacing="1" cellpadding="1">
      <tr>
        <th>What's Required</th>
        <th>Your Server</th>
        </tr>
      <tr>
        <td width="26%">PHP 5.2.x</td>
        <td width="30%" align="center"><?php if(phpversion()>="5.2.0"){
			echo phpversion();
			$php = 1;
		}else{
			echo "False";
			$php = 0;
		}?></td>
        </tr>
      <tr>
        <td>Writeable Directory</td>
        <td align="center"><?php 
		$file = "index.php";
		$file_directory = dirname($file);
		if(is_writable($file_directory)=="1"){
			echo "Writable";
			$write = 1;
		}
		elseif(is_writable($file_directory)=="0"){
			echo "Not Writable";
			$write = 0;
		}
		?></td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td align="center">&nbsp;</td>
        </tr>
      <tr>
        <td>Final Verdict</td>
        <td align="center"><?php if($php == 1 && $write == 1){
			?>Congratulations! Your server can run reSys<?php $check = 1;
		}else{
			?>We're sorry. You cannot run reSys at this time. Please fix what is mentioned above and try again."<?php $check = 0; } ?></td>
        </tr>
    </table>
    <br />
    <br />
    <?php if($check==1){ ?>
    <p>Alright! Since your server can support quickSys we're ready to move onto the next step!</p>
    <p align="center"><a href="install.php?step=2">Click here to move to Step 2!</a></p>
    <?php } elseif($check==0) { ?>
    <p>We're sorry that you couldn't continue. Please try again after you've made the appropriate revisions.</p>
    <p align="center"><a href="install.php?step=1">Click here to run the check again.</a></p><?php } ?>
    <br />
    <?php } ?>
    <?php }
	if($_GET['step']=="2"){ 
		if(isset($_GET['do'])){ 
			if($_GET['do']=="1") { ?>
    <h2>Copy the config files!</h2>
    <p>To complete installation, copy the code below into a new file called: <br /><br /><span style="font-family: 'Courier New', Courier, monospace">config.php</span></p>
    <textarea name="" cols="100" rows="6">
$sitedatas = array ( 'sitename' => '<?php echo $_POST['sitename']; ?>',
'site tagline' => '<?php echo $_POST['sitetagline']; ?>',
'theme' => 'default',
'enabled' => '1');</textarea>

	<br />
	
    <p><br />
    </p>
	<p><span style="font-family: 'Courier New', Courier, monospace">admin/users/users.php</span></p>
    <textarea name="" cols="100" rows="9">
    <p>&lt;?php /*<br />
Users Database (DO NOT MODIFY)</p>
    <p>(c) quickSys Matthew Sowden 2011~2012 All rights Reserved.</p>
    <p>*/<br />
      $users = array( '1' =&gt; array ( 'user' =&gt; '<?php echo $_POST['user']; ?>', <br />
      'pass' =&gt; '<?php echo md5($_POST['pass']); ?>', <br />
      'email' =&gt; '<?php echo md5($_POST['email']); ?>' ) );</p>
    </textarea>
    <?php } }else{ ?>
   	<h2>Config Setup</h2>
    <p>Next up is setting up the config file!</p>
    <br />
    <br />
    <form id="form1" name="form1" method="post" action="install.php?step=2&do=1">
    <table width="100%" border="0" cellspacing="5" cellpadding="0">
  <tr>
    <td><h3>Site Information</h3></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Site Name:</td>
    <td><label for="sitename"></label>
      <input name="sitename" type="text" id="sitename" value="A Cool Site!" /></td>
  </tr>
  <tr>
    <td>Site Tagline:</td>
    <td><label for="sitetagline"></label>
      <input name="sitetagline" type="text" id="sitetagline" value="An awesome quickSyS Site!" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><h3>Admin Account Info</h3></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Username:</td>
    <td><label for="user2"></label>
      <input name="user" type="text" id="user2" value="admin" /></td>
  </tr>
  <tr>
    <td>Password:</td>
    <td><label for="pass"></label>
      <input name="pass" type="password" id="pass" value="password" /></td>
  </tr>
  <tr>
    <td>E-mail:</td>
    <td><label for="email"></label>
      <input name="email" type="text" id="email" value="example@mattsowden.com" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input type="submit" name="submit2" id="submit2" value="Submit" /></td>
  </tr>
    </table>
    </form>
    <?php } } ?>
    </div>
</div>
</div>
</body>
</html>
