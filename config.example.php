<?php /*
quickSys Configuration File

(c)2011~2012 Matthew Sowden (all rights reserved)
http://quicksys.mattsowden.com

DO NOT REMOVE THIS COPYRIGHT.

quickSyS Version: v0.1

*/

$sitedatas = array ( 'sitename' => 'Example Site',
					 'site tagline' => 'Example Tagline',
					 'theme' => 'default',
					 'enabled' => '1');